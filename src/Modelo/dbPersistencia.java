/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;

/**
 *
 * @author Juan Antonio
 */
public interface dbPersistencia {
    //AQUI SE DECLARARAN COMO SI FUERAN BOTONES
    public void insertar(Object objecto) throws Exception;
    public void actualizar(Object objecto)throws Exception;
    public void habilitar(Object objecto)throws Exception;
    public void deshabilitar(Object objecto)throws Exception;
    //AQUI SE DECLARARAN COMO UNA LISTA 
    public boolean isExiste(String codigo)throws Exception;
    public ArrayList listar()throws Exception;
    public ArrayList listar(String criterio)throws Exception;
    //ESTE BUSCARA YA SEA POR ID O POR CODIGO
    public Object buscar(String objecto)throws Exception;
}
